import React, { Component } from 'react';
import Search from './components/Search';
import City from './components/City';
import { BrowserRouter, Route, Switch} from 'react-router-dom';

class App extends Component {
	state = { }

	render() {
		return (
			<BrowserRouter>
				<div>
					
					<Switch>
						<Route exact path='/' component={Search} />
						<Route path="/city/:title_id" component={City} />
					</Switch>   
				</div>
			</BrowserRouter>
			
		);
	}
}

export default App;
