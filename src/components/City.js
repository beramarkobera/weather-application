import React, { Component } from 'react';
import axios from 'axios';
import WeatherInfo from './WeatherInfo';

class City extends Component {
    state = {
        city: {},
        weatherInfo: { 
            consolidated_weather: []
        }
    };

    componentDidMount() {
        axios.get('https://cors.io/?https://www.metaweather.com/api/location/search/?query='+this.props.match.params.title_id)
            .then( response => {                  
                this.setState({
                    city: response.data[0]
                })  
                axios.get('https://cors.io/?https://www.metaweather.com/api/location/'+response.data[0].woeid)
                    .then( response => {
                        this.setState({
                            weatherInfo: response.data
                        });
                    })
            })
            .catch( error => {
                console.log("ERROR: "+error);
            })
    }
    
    render() {
        const {title, latt_long, location_type, woeid} = this.state.city;
        const {consolidated_weather, sun_rise, sun_set} = this.state.weatherInfo;

        const cwList = consolidated_weather.map(item => {
            console.log('item: ', item)
            return (
                <div key={item.id}>
                    <WeatherInfo info={item} />
                </div>
            )
        })
        return (
            <div>
                <h4>{title}</h4>
                <div>
                    {cwList}
                </div>
            
            </div>
            
        )
    }
}

export default City;

