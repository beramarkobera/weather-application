import React, { Component } from 'react'
import axios from 'axios';
import {Link} from 'react-router-dom';

class Search extends Component {
	_isMounted = false;

	state = {
		cities: []
	}

	componentWillUnmount() {
        this._isMounted = false;
    }

	handleChange = (e) => {
		this._isMounted = true;
		const {value} = e.target;
		
		if(value !== '') {
			axios.get('https://cors.io/?https://www.metaweather.com/api/location/search/?query='+value)
				.then( response => {
					if(this._isMounted) {
						this.setState({
							cities: response.data.slice().map( city => city.title)
						})
					}
					
				})
				.catch( error => {
					console.log(error);
				})
			
		}
	}
	
	render() {
		const links = this.state.cities.map( (title, index) => {
			return (
				<li key={index}>
					<Link to={"/city/"+title}> {title} </Link>
				</li>
			)
		})

		return (
			<div>
				<label> Search: </label>
				<input 
					type="text" 
					placeholder="Enter city name..."
					onChange={this.handleChange}
				/>
				<br/>
				<ul>
					{links}
				</ul>
			</div>
		)
	}
}

export default Search;
