import React from 'react'

const getWindDirectionStyle = (dir) => {
    const startAngle = 90+dir;
    return {transform: 'rotate('+startAngle+'deg)'};
}



export default function WeatherInfo(props) {

    const {air_pressure, humidity, min_temp, max_temp, wind_direction_compass, wind_direction, wind_speed, weather_state_abbr, weather_state_name} = props.info;
    const icon_src = 'https://www.metaweather.com/static/img/weather/' + weather_state_abbr + '.svg';

    return (
        <div className="info-holder">
            <h5> </h5>
            <div>
                <img src={icon_src} alt={weather_state_name} width="64px"></img>
            </div>
            <div>
                <h5> Temperature </h5>
                <p>
                    <span>Max: </span>{max_temp.toFixed(2)} <span>℃</span> <br/>
                    <span>Min: </span>{min_temp.toFixed(2)} <span>℃</span>
                </p>
            </div>
            
            <p>{air_pressure}</p>
            <p>{humidity}</p>
            <div>
                <p>
                    <img 
                        src='https://cdn.pixabay.com/photo/2013/07/12/16/28/arrow-150966_960_720.png' 
                        alt={"wind direction is " + wind_direction_compass}
                        width="64px" 
                        style={getWindDirectionStyle(wind_direction)}
                    ></img>

                    <span>Speed: {(wind_speed * 1.6).toFixed(2) + ' km/h'}</span>
                </p>
                
            </div>
            
        </div>
    )
}
